<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id'          => 'dre_service',
    'title'       => [
        'en' => '<img src="../modules/bender/dre_service/out/img/favicon.ico" title="Bodynova Service Modul">odynova Service Modul',
        'de' => '<img src="../modules/bender/dre_service/out/img/favicon.ico" title="Bodynova Service Modul">odynova Service',
    ],
    'description' => [
        'en' => 'service scripts',
        'de' => 'Service Modul führt Datenbank Service Skripte aus',
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0',
    'author'      => 'Bodynova GmbH',
    'url'         => 'https://bodynova.de',
    'email'       => 'support@bodynova.de',
    'controllers'       => [
    ],
    'extend'      => [
    ],
    'templates'   => [
    ],
    'blocks'      => [],
    'settings'    => [],
];