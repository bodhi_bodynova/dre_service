<?php

include_once('../../../bootstrap.php');

use OxidEsales\Eshop\Core\DatabaseProvider;

$vaeter = getDeaktiveVaeterMitAktivenVarianten();

echo "Anzahl deaktivierter Väter mit aktiven Varianten gefunden: ".count($vaeter).'<br/>';

foreach ($vaeter as $vater){
    activateArtikelByOxid($vater['ParentOxid']);
}
echo '<pre>';
print_r($vaeter);
echo '</pre>';




$aktiveVäterOhneAktiveVarianten = getAktiveVaeterMitDeaktivenVarianten();

echo 'Aktive Väter ohne aktive Varianten: ' . count($aktiveVäterOhneAktiveVarianten) .'<br/>';

foreach($aktiveVäterOhneAktiveVarianten as $oxid){
    deActivateArtikelByOxid($oxid['OXID']);
}

echo '<pre>';
print_r($aktiveVäterOhneAktiveVarianten);
echo '</pre>';


/*

$vatermitdeaktivemKind = '0004ac5f13b9d4636.96738259';

$test = getAllChildren($vatermitdeaktivemKind);

echo 'test<br/>';
echo 'Anzahl Active Children: '. getAnzahlActive($vatermitdeaktivemKind)[0]['Anzahl'];

echo '<pre>';
print_r($test);
echo '</pre>';


$aktiveVäterOhneAktiveVarianten = getAktiveVaeterMitDeaktivenVarianten();

echo '<pre>';
print_r($aktiveVäterOhneAktiveVarianten);
echo '</pre>';

/* mit children
$testid = '5f6bfa216a049634cc38465f373925fd';

/* standardartikel
//$testid = '0014784ef409d92d4.29110305';

echo 'testid = '. $testid.'<br/>';
echo 'Childartnums: '. baueChildartnums($testid).'<br/>';

$kinder = baueChildartnums($testid);
$alleVäteroderStandard = getAlleVaeterOderStandard();

foreach($alleVäteroderStandard as $vater){
    setChildartnums($vater['OXID'], baueChildartnums($vater['OXID']));
}

echo '<pre>';
print_r($alleVäteroderStandard);
echo '</pre>';
*/
// Funktionen:


/**
 * Gibt ein Array zurück mit deaktivierten Vätern, die aktive Varianten haben
 *
 * @return array
 * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
 * @throws \OxidEsales\Eshop\Core\Exception\DatabaseErrorException
 */
function getDeaktiveVaeterMitAktivenVarianten()
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $qv = "SELECT " .
        "parent.OXID as ParentOxid, " .
        "parent.OXPARENTID as Parentparentid, " .
        "parent.OXACTIVE as ParentActive, " .
        "parent.OXARTNUM as ParentArtnum, " .
        "parent.OXVARCOUNT as ParentVarcount, " .
        "parent.childartnums as ParentChildartnums, " .
        "child.OXID as childoxid, " .
        "child.OXPARENTID as childparentid, " .
        "child.OXACTIVE as childactive, " .
        "child.OXARTNUM as childartnum, " .
        "child.childartnums as childartnums " .
        "FROM oxarticles as parent " .
        "right JOIN oxarticles as child " .
        "ON (parent.OXID = child.OXPARENTID) " .
        "where (child.OXPARENTID > 0 and child.OXACTIVE = 1 and parent.OXACTIVE = 0) " .
        "group BY parent.OXID";
    return $db->getAll($qv);
}

/**
 * Gibt ein Array zurück mit deaktivierten Vätern, die aktive Varianten haben
 *
 * @return array
 * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
 * @throws \OxidEsales\Eshop\Core\Exception\DatabaseErrorException
 */
function getAktiveVaeterMitDeaktivenVarianten()
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $qv = "SELECT parent.OXID FROM oxarticles AS parent WHERE parent.OXPARENTID = '' AND parent.OXACTIVE = 1 AND parent.OXVARCOUNT > 0 AND ( SELECT COUNT(*) FROM oxarticles WHERE OXPARENTID = parent.OXID AND OXACTIVE = 1 ) = 0";

    return $db->getAll($qv);
}

/**
 * deaktiviert Artikel
 * @param $oxid
 * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
 * @throws \OxidEsales\Eshop\Core\Exception\DatabaseErrorException
 */
function deaktiviereArtikelByOxid($oxid)
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $qv = "Update oxarticles set OXACTIVE = ? WHERE OXID = ?";
    $db->execute($qv,array(0,$oxid));
}

function getChildArtnums($oxid)
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
}

function getActiveChildren($oxid)
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $query = "SELECT OXID,OXARTNUM,OXACTIVE from oxarticles WHERE OXPARENTID = ? AND OXACTIVE = 1";
    return $db->getAll($query,array($oxid));
}

function getArtnum($oxid)
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $query = "SELECT OXARTNUM from oxarticles WHERE OXID = ?";
    return $db->getAll($query, array($oxid));
}

function baueChildartnums($oxid)
{
    if(count(getActiveChildren($oxid)) > 0){
        $returnarray = null;
        foreach(getActiveChildren($oxid) as $kind){
            $returnarray[] = $kind['OXARTNUM'];
        }
        array_push($returnarray,getArtnum($oxid)[0]['OXARTNUM']);
        return implode(" ", $returnarray);
    }else{
        return implode(" ", getArtnum($oxid)[0]);
    }
}

function getAlleVaeterOderStandard()
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $query = "SELECT OXID,OXARTNUM,childartnums from oxarticles WHERE OXACTIVE = 1 AND OXPARENTID=''";
    return $db->getAll($query);
}

function setChildartnums($oxid, $string)
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $query = "update oxarticles set childartnums = ? WHERE OXID = ?";
    $db->execute($query, array($string, $oxid));
}

function activateArtikelByOxid($oxid)
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $query = "update oxarticles set OXACTIVE = 1 WHERE OXID = ?";
    $db->execute($query, array($oxid));
}

function deActivateArtikelByOxid($oxid)
{
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $query = "update oxarticles set OXACTIVE = 0 WHERE OXID = ?";
    $db->execute($query, array($oxid));
}

function getAllChildren($oxid){
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $query = "SELECT OXID, OXPARENTID, OXACTIVE, OXARTNUM From oxarticles WHERE OXPARENTID = ?";
    return $db->getAll($query,array($oxid));
}

function getAnzahlActive($parentid){
    $db = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    $query = "SELECT SUM(OXACTIVE) as Anzahl  From oxarticles WHERE OXPARENTID = ?";
    return $db->getAll($query, array($parentid));
}